var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var mainCore = require('../src/Controllers/mainCore');
const fs = require("file-system");
var should = chai.should();

chai.use(chaiHttp);

describe('mainCore', function() {
    it('Nova regra', function(done) {

        var novaRegra = {
            type: "dia",
            date: "04-03-2021",
            startTime: "15:00:00",
            endTime: "16:00:00"
        };

        chai.request(server)
            .post('/api/v1/rules')
            .send(novaRegra)
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.should.have.property('message');
                done();
            })

    })

    it('Pegar todos as regras', function(done) {
        chai.request(server)
            .get('/api/v1/rules')
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.should.have.property('message');
                done();
            })
    })

    it('Exibir lista de horarios disponiveis', function(done) {

        var rangeTime = {
            startDate: "14-11-2019",
            endDate: "02-03-2020"
        };

        chai.request(server)
            .post('/api/v1/availableTimes')
            .send(rangeTime)
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.should.have.property('message');
                done();
            })
    })
});