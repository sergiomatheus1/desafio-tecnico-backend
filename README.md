
## Endpoints:

### Cadastro de regra de atendimento

O cadastro de regras de horário para atendimento deve possibilitar que se disponibilize intervalos de horário para consulta, possibilitando regras para:

    - Um dia especifico, por exemplo: estará disponível para atender dia 25/06/2018 nos intervalos de 9:30 até 10:20 e de 10:30 até as 11:00
    URL:    http://localhost:3000/api/v1/rules
    HTTPMETHOD: Post
    BODY: {
                "type": "dia",
                "date": "25/06/2018",
                "startTime": "09:30:00",
                "endTime": "10:20:00"
            }
            Um Após o outro em requisições separadas
            {
                "type": "dia",
                "date": "25/06/2018",
                "startTime": "10:30:00",
                "endTime": "11:00:00"
            }
    - Diáriamente, por exemplo: estará disponível para atender todos os dias das 9:30 até as 10:10
        URL:    http://localhost:3000/api/v1/rules
        HTTPMETHOD: Post
        BODY: {
                    "type": "diario",
                    "daysWeek": "Monday-Sunday",
                    "startTime": "09:30:00",
                    "endTime": "10:10:00"
                }

    - Semanalmente, por exemplo: estará disponível para atender todas segundas e quartas das 14:00 até as 14:30
    URL:    http://localhost:3000/api/v1/rules
    HTTPMETHOD: Post
    BODY:  {
            "type": "semana",
            "daysWeek": "satuday",
            "startTime": "12:00:00",
            "endTime": "13:00:00"
        }

### Apagar regra

Este metódo deve ser capaz de de apagar uma regra especifica criada pelo endpoint descrito em "Cadastro de regra de atendimento".
    URL:    http://localhost:3000/api/v1/rules
    HTTPMETHOD: Delete
    BODY:  {
        "id": "8"
        }


### Listar regras

O metódo de listar deve retornar as regras de atendimento criadas pelo endpoint descrito em "Cadastro de regra de atendimento".
    URL:    http://localhost:3000/api/v1/rules
    HTTPMETHOD: GET
    BODY:  Não Utiliza

### Horários disponíveis

Este endpoint deve retornar os horários disponíveis, baseado nas regras criadas anteriormente, considerando um intervalo de datas informadas na requisição.

    URL:    http://localhost:3000/api/v1/availableTimes
    HTTPMETHOD: Post
    BODY:  {
            "startDate": "14-11-2019",
            "endDate": "02-03-2020"
        }


## Documentacao

    Feito com SwaggerUI http://localhost:3000/api-docs/ é pássivel de criar as requisições pela propria documentação

## Testes

    Testes Feitos com Mocha , chai e chai-http Comando para rodar npm run test
