const express = require("express")();
router = require("express").Router();
const rules = require("../Controllers/mainCore");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
swaggerDocument = require("../../swagger.json");

express.use(bodyParser.json());
express.use(
  bodyParser.urlencoded({
    extended: true
  })
);
express.use("/api/v1", router);
express.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

router
  .route("/rules")
  .get(rules.listRules)
  .post(rules.addRule)
  .delete(rules.deleteRule);

router.route("/availableTimes").post(rules.availableTimes);

module.exports = express;
