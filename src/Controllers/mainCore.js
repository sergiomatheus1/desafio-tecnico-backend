const rangeDays = require("../Services/rangeDays");
const controllerFS = require("./controllerFS");
const convertISODateFormat = require("../Services/dateFormat");
const getDiaSemana = require("../Services/getDiaSemana");

let mainCore = (module.exports = {});

mainCore.addRule = async(req, res) => {
    try {
        let contents = await controllerFS.readFileSync();
        let id = Object.keys(contents.rules).length;
        for (let prop in contents.rules) {
            if (contents.rules[prop].type === "diario") {
                if (req.body.type === "dia" ||
                    req.body.type === "diario" ||
                    req.body.type === "semana") {

                    if (contents.rules[prop].startTime === req.body.startTime ||
                        contents.rules[prop].endTime === req.body.endTime) {

                        return res.status(400).send({ error: `Este horário já possue uma regra cadastrada.` });

                    }
                } else {
                    return res
                        .status(400)
                        .send(JSON.stringify({ error: `RequestBody Incorreto` }));
                }
            } else if (contents.rules[prop].type === "dia") {
                if (req.body.type === "diario") {
                    if (
                        contents.rules[prop].startTime === req.body.startTime ||
                        contents.rules[prop].endTime === req.body.endTime
                    ) {
                        return res
                            .status(400)
                            .send({ error: `Este horário já possue uma regra cadastrada.` });
                    }
                } else if (req.body.type === "semana") {
                    let daysReq = req.body.daysWeek.split("-");
                    let ruleDay = convertISODateFormat(contents.rules[prop].date);
                    let data = new Date(ruleDay).toDateString().substring(0, 3);
                    daysReq.map(day => {
                        day = day.substring(0, 3);
                        if (data === day) {
                            return res
                                .status(400)
                                .send({ error: `Este horário já possue uma regra cadastrada.` });
                        }
                    });
                } else if (req.body.type === "dia") {
                    if (req.body.date === contents.rules[prop].date) {
                        if (
                            req.body.startTime === contents.rules[prop].startTime ||
                            req.body.endTime === contents.rules[prop].endTime
                        ) {
                            return res
                                .status(400)
                                .send({ error: `Esta data já possue uma regra cadastrada neste horário.` });
                        }
                    }
                } else {
                    return res
                        .status(400)
                        .send(JSON.stringify({ error: `RequestBody Incorreto` }));
                }
            } else if (contents.rules[prop].type === "semana") {
                if (req.body.type === "dia") {
                    let reqDay = convertISODateFormat(req.body.date);
                    let data = getDiaSemana(reqDay);
                    let daysRule = contents.rules[prop].daysWeek.split("-");
                    daysRule.map(day => {
                        if (data === day) {
                            return res
                                .status(400)
                                .send({ error: `Esta data já possue uma regra semanal cadastrada neste horário` });
                        }
                    });
                } else if (req.body.type === 'diario') {
                    let daysReq = req.body.daysWeek.split('-');
                    let daysRule = contents.rules[prop].daysWeek.split('-');
                    daysReq.map(value1 => {
                        daysRule.map(value2 => {
                            if (value1 === value2) {
                                return res.status(200).send(`time conflict, rule not added, weekly = weekly`);
                            }
                        })
                    })

                } else if (req.body.type === "semana") {
                    let daysReq = req.body.daysWeek.split("-");
                    let daysRule = contents.rules[prop].daysWeek.split("-");
                    daysReq.map(value1 => {
                        daysRule.map(value2 => {
                            if (value1 === value2) {
                                return res
                                    .status(400)
                                    .send({ error: `Esta semana já possue uma regra cadastrada neste horário` });
                            }
                        });
                    });
                } else {
                    return res
                        .status(400)
                        .send(JSON.stringify({ error: `RequestBody Incorreto` }));
                }
            }
        }
        contents.rules[++id] = req.body;
        let content = await controllerFS.writeFile("Success, rule added", contents);

        res.status(200).send({ message: content });
    } catch (e) {
        return res.status(400).send(JSON.stringify({ error: e.message }));
    }
};

mainCore.listRules = async(req, res) => {
    let contents = await controllerFS.readFileSync();

    res.status(200).send({ message: contents.rules });
};

mainCore.deleteRule = async(req, res) => {
    try {
        let contents = await controllerFS.readFileSync();
        let id = req.body.id;
        if (contents.rules[id] !== undefined) {
            contents.rules[id] = undefined;
            let result = await controllerFS.writeFile(
                "Regra de horário deletada com sucesso",
                contents
            );
            res.status(200).send({ message: result });
        } else {
            res.status(400).send({ error: "Regra de horário não encontrada" });
        }
    } catch (e) {
        return res.status(400).send(JSON.stringify({ error: e.message }));
    }
};

mainCore.availableTimes = async(req, res) => {
    try {
        let vetResult = [];
        let objectResult = {};

        let contents = await controllerFS.readFileSync();
        let startDate = convertISODateFormat(req.body.startDate);
        let endDate = convertISODateFormat(req.body.endDate);

        startDate = new Date(startDate);
        endDate = new Date(endDate);

        let vetRange = await rangeDays(startDate, endDate);

        vetRange.map(value => {
            for (let prop in contents.rules) {
                if (value === contents.rules[prop].date) {
                    if (objectResult[value] === undefined) {
                        objectResult[value] = [{
                            start: contents.rules[prop].startTime,
                            end: contents.rules[prop].endTime
                        }];
                    } else {
                        objectResult[value].push({
                            start: contents.rules[prop].startTime,
                            end: contents.rules[prop].endTime
                        });
                    }
                }
            }
        });

        vetRange.map(data => {
            if (objectResult[data] !== undefined) {
                vetResult.push({ day: data, intervals: objectResult[data] });
            }
        });
        res.status(200).send({ message: vetResult });
    } catch (e) {
        return res.status(400).send(JSON.stringify({ error: e.message }));
    }
};